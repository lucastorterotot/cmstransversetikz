# CMSTransverseTikZ
Get illustrative event displays in the CMS transverse plane easily with TikZ!

## Introduction

## Installation
You can use `CMSTransverseTikZ` files to add in your documents by using `\input{...}` or as a `TeX` package.
In this last case, check how and where to include new packages on your machine.
You may have to do before cloning:
```
mkdir -p ~/texmf/tex/latex ; cd ~/texmf/tex/latex
```

In any case, you now have to clone this repository:

- with `ssh` if you have a key:
```
git clone git@gitlab.com:lucastorterotot/cmstransversetikz.git -b last_release
```

- else with `https`:
```
git clone https://gitlab.com/lucastorterotot/cmstransversetikz.git -b last_release
```

This will automatically point you to the `last_release` branch to have to most up-to-date version of this package while skipping development steps.

## Usage

When using this package, please acknowledge it with [the provided citation](https://gitlab.com/lucastorterotot/cmstransversetikz/-/blob/master/CMSTransverseTikZ.bib). Thanks!

See the examples to get how to draw event displays.
To load the macros, follow the instructions below.

### As a TeX package (recommanded)
In your document preamble, just add
```
\usepackage{cmstransversetikz}
```
and you're ready!

### As additional files (easier to use)
Make sure you load the following packages:

- `xcolor`;
- `tikz`;
- `ifthen`.


Then, before you draw the first image or in your preamble, add
```
\input{<PATH TO THIS REPOSITORY>/CMSTransverseTikZ.tex}
```
and you're ready!
